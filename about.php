<?php

$pageTitle = 'About Me · Max Bailey';
$page = 'about';
require_once('inc/header.php');

?>
	<section class="center about">
		<div class="container">
			<h1>Brief Overview</h1>
			<hr>
			<p>I'm a young developer who has thrived in an incredibly competitive industry. I started my professional career at the age of 13, when I was hired by a small freelance agency out of the United Kingdom. Now I am currently a developer at the largest American owned student information system in the world at age 18. This is a bit about who I am, and how I got here.</p>
		</div>
	</section>
	<section class="center boxes">
		<div class="container">
			<h1>My Skillset</h1>
			<hr>
			<div class="box">
				<h2>Design</h2>
				<p>Over the years I have refined my style to be clean, minimal, and beautiful. My app designs tackle very complex problems with visually simple solutions.</p>
			</div>
			<div class="box">
				<h2>HTML</h2>
				<p>Often overlooked, HTML is incredibly important to the development process. Clean, meaningful, and semantic HTML is the base of a great product.</p>
			</div>
			<div class="box">
				<h2>Sass</h2>
				<p>Well crafted CSS can make a website or app truly amazing. Having the ability to change the look, feel, and layout without touching any logic is incredibly powerful.</p>
			</div>
			<div class="box">
				<h2>Photoshop</h2>
				<p>Even though a majority of my design is done in the browser, Photoshop remains an essential design tool. Anything from wireframes, to high fidelity mockups.</p>
			</div>
			<div class="box">
				<h2>Sketch App</h2>
				<p>With the recent advances in high DPI display technology, it's more important than ever to make all assets work on all displays. Sketch is my go to tool for SVGs.</p>
			</div>
			<div class="box">
				<h2>Version Control</h2>
				<p>I use Git on a daily basis, and on all of my projects. I believe Git is a very powerful tool to master, and I even use it to version my text editor settings and plugins!</p>
			</div>
		</div>
	</section>
	<section class="center quote">
		<div class="container">
			<blockquote>“The best way to predict the future is to create it.”</blockquote>
			<cite>— Peter Drucker</cite>
		</div>
	</section>
<?php require_once('inc/footer.php'); ?>
