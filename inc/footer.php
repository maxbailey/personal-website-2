	<?php if($page != 'home') { ?>
	<footer>
		<div class="container">
			<p>Made with love in Minnesota</p>
			<a href="http://dribbble.com/maxbailey" ><span class="fa fa-dribbble"></span></a>
			<a href="http://twitter.com/maxbailey"><span class="fa fa-twitter"></span></a>
			<a href="http://github.com/maxbailey"><span class="fa fa-github"></span></a>
			<a href="http://linkedin.com/in/maxwbailey"><span class="fa fa-linkedin"></span></a>
		</div>
	</footer>
	<?php } ?>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script type="text/javascript">
		$('#menu').click(function() {
			$('.menu').toggleClass('show');
		});
	</script>
</body>
</html>
