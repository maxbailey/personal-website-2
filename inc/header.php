<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $pageTitle ?></title>
	<link rel="stylesheet" href="css/global.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!--[if lt IE 9]><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<script src="//use.typekit.net/ipc5vqa.js"></script>
	<script>try{Typekit.load();}catch(e){}</script>
</head>
<body>
	<header<?php if($page === 'home'){echo ' class="home"';} ?>>
		<div class="container">
			<a href="/" class="logo">Max Bailey</a>
			<nav class="mobile">
				<a href="#" id="menu"><span class="fa fa-bars"></span> Menu</a>
			</nav>
			<nav>
				<a href="/"<?php if($page === 'home'){echo ' class="active"';} ?>>Home</a>
				<a href="/about"<?php if($page === 'about'){echo ' class="active"';} ?>>About Me</a>
				<a href="/portfolio"<?php if($page === 'portfolio'){echo ' class="active"';} ?>>My Portfolio</a>
				<a href="/resume.pdf" target="_blank">Résumé</a>
				<a href="mailto:hello@maxbailey.me" class="button">Contact Me</a>
			</nav>
		</div>
	</header>
	<div class="menu">
		<a href="/">Home</a>
		<a href="/about">About Me</a>
		<a href="/portfolio">My Portfolio</a>
		<a href="/resume.pdf" target="_blank">Résumé</a>
		<a href="mailto:hello@maxbailey.me">Contact Me</a>
	</div>
