<?php

$pageTitle = 'Max Bailey · Front End Developer &amp; Tech Enthusiast';
$page = 'home';
require_once('inc/header.php');

?>
	<div class="hero">
		<div class="container">
			<h1 class="one">Designer.</h1>
			<h1 class="two">Developer.</h1>
			<h1 class="three">Tech Enthusiast.</h1>
		</div>
	</div>
<?php require_once('inc/footer.php'); ?>
