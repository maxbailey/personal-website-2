<?php

$pageTitle = 'My Portfolio · Max Bailey';
$page = 'portfolio';
require_once('inc/header.php');

?>
	<section class="center no-border">
		<div class="container">
			<h1>Selected Works</h1>
			<hr>
		</div>
	</section>
	<section class="portfolio">
		<div class="container">
			<div class="content right">
				<h2>MKBHD + Squarespace <span class="badge">Coming Soon!</span></h2>
				<p>Marques Brownlee, also known as "MKBHD", is one of the largest technology channels on YouTube. I have been fortunate enough to have worked with Marques to create the perfect website to promote his content, tell his story, and even sell his merchandise. <a href="http://squarespace.com" target="_blank">Squarespace</a> was generous enough to provide the CMS for this website, and also volenteered to code the website as well. This website reaches millions of his subscribers.</p>
				<a href="http://mkbhd.com" class="visit" target="_blank">Visit Website<span class="fa fa-arrow-right"></span></a>
			</div>
			<img src="img/project/mkbhd/banner.jpg">
		</div>
	</section>
	<section class="portfolio">
		<div class="container">
			<div class="content right">
				<h2>Trello Concept</h2>
				<p>This project was purely a concept. I've been a long time Trello user, and I wanted to go ahead and give their homepage a makeover. This was prior to their 2014 redesign. I tried to keep the Trello branding consistant to what it currently it. I coded the mockup as well, and it can be viewed below. It is a fully responsive landing page, that uses only assets from Trello, or ones I created myself.</p>
				<a href="/trello/" class="visit" target="_blank">Visit Website<span class="fa fa-arrow-right"></span></a>
			</div>
			<img src="img/project/trello/banner.jpg">
		</div>
	</section>
	<section class="portfolio">
		<div class="container">
			<div class="content right">
				<h2>EJM Pipe Services <span class="badge">Coming Soon!</span></h2>
				<p>EJM is a multi-million dollar construction company based out of Minnesota, that provides their services all across the United States. They were in need of a website renovation, so that's exactly what we did. Together we created an amazing new website that meets all of their needs. With the rapid growth of the technology industry, things like online job applications are more essential than ever. EJM's new website brought their online presence to the next level.</p>
				<a href="http://www.ejmpipe.com/" class="visit" target="_blank">Visit Website<span class="fa fa-arrow-right"></span></a>
			</div>
			<img src="img/project/ejm/banner.jpg">
		</div>
	</section>
	<section class="branding mkbhd">
		<div class="container">
			<h1>MKBHD Branding <small>Created by Marques Brownlee</small></h1>
			<hr>
			<img src="img/project/mkbhd/mark.png">
			<img src="img/project/mkbhd/pattern.png">
			<img src="img/project/mkbhd/typo.png">
			<img src="img/project/mkbhd/colors.png">
		</div>
	</section>
	<section class="branding hd">
		<div class="container">
			<h1>Concept Branding</h1>
			<hr>
			<img src="img/project/hd/lockup.png">
			<img src="img/project/hd/mark.png">
			<img src="img/project/hd/typo.png">
			<img src="img/project/hd/colors.png">
		</div>
	</section>
	<section class="branding polydawn">
		<div class="container">
			<h1>Polydawn Branding</h1>
			<hr>
			<img src="img/project/polydawn/brand.png" style="width:100% !important;">
		</div>
	</section>
	<section class="branding mmk">
		<div class="container">
			<h1>MMK Branding</h1>
			<hr>
			<img src="img/project/mmk/logored.png">
			<img src="img/project/mmk/logo.png">
			<img src="img/project/mmk/colors.png">
			<img src="img/project/mmk/typo.png">
		</div>
	</section>
	<div class="cta">
		<div class="container">
			<h1><strong>Interested?</strong> Want to see more?</h1>
			<a href="http://dribbble.com/maxbailey" target="_blank" class="button">View my Dribbble</a>
		</div>
	</div>
<?php require_once('inc/footer.php'); ?>
